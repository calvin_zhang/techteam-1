README

##Download/Install Dropbox onto target system##
	 
	 Site:  www.dropbox.com
	 User:	techteam-dropbox@greatergood.com
	 PW:	See teammate for password.

	 NOTE:  The free version of Dropbox has a limit of 2 GB free storage.  The storage can be increased for a price.  See website for details.

##Download/Install KeePassX 2 onto target system##
	 
	 Site:  https://www.keepassx.org/

	 NOTE:  You must use KeePassX 2, NOT KeePassX*.  The older version may corrupt the database.

##Configure KeePassX 2##
	 
	 Under settings set the "Remember last database used".  Use "Open database" and navigate to the team Dropbox where you will see the GreaterGood-TechTeam database.  Reach out to one of your teammates for the master password.  Once the password has been entered you should see groups and within those groups are the various accounts.

##HOWTO##

	This tools needs little explanation, and if you have questions refer to the website for details on how to create groups and accounts, edit and etc..

##Lock File##

	Dropbox creates a lock file, and updates the user's Desktop that it is locked.  You can read only, continue or cancel.  If something goes awry, we can delete the lock file.

##Ideal Usage##

	Two to four master users should avoid most lock file issues.  We can delete the lock file on Dropbox in extreme cases.  The file is backed up locally in the event a user destroys the database.
