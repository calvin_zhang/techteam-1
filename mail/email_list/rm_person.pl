#!/usr/bin/perl

if($ARGV[0]) {
    if(open(X,$ARGV[0])) {
        $match=0;
        @d = <X>;
        chomp @d;
        close X;
        if(open(Y,">$ARGV[0]")) {
            foreach (@d) {
                if(/$ARGV[1]/i) {
                    $match++;
        	    print "removing $ARGV[1] from $ARGV[0]...\n";
                } else {
                    print Y $_,"\n";
                }
            }
            close Y;
	    if ($match>1) {
            	print "done. found matches on $match lines.\n";
	    }
            exit 0;
        } else {
            print "error: could not open file \"$ARGV[0]\" for writing!\n";
            exit 2;
        }
    } else {
        print "error: could not open file \"$ARGV[0]\" for reading!\n";
        exit 2;
    }
} else {
    print "usage: rm_person.pl filename email_address \n\n";
    exit 1;
}
